## Think-Element-admin

1.异常处理 Handle 在配置文件中修改为

```
  'exception_handle'       => '\\app\\common\\exception\\ExceptionHandle',
```

ExceptionHandle 将全面接管系统的异常/错误,全部返回 json 信息,见 render 方法
report 会将异常报告给指定单位,默认为钉钉,可在环境变量中配置相关配置项

2.对 DB--Connection 进行了少量优化(加入异常日志相关功能)

3.加入事件系统,触发时间只需要调用助手函数`triggerEvent`


## Environment

> 1.  PHP 推荐使用 PHP7 以达到最优效果；

- Nginx配置

```
server {
    listen 80;
    listen 443 ssl http2;
    server_name  ;
    root "/home/vagrant/code/think-vue-element-admin/ThinkAdmin";

    index index.html index.htm index.php;

    charset utf-8;

    add_header X-Powered-Host $hostname;

    fastcgi_hide_header X-Powered-By;

    access_log off;
    error_log  /var/log/nginx/host-error.log error;

    sendfile off;

    client_max_body_size 100m;
    location / {
      if (-f $request_filename) {
         break;
      }
      if ($request_filename ~* "\.(txt|js|ico|gif|jpe?g|bmp|png|css)$") {
          break;
      }

      if ($request_method = OPTIONS) {
          add_header 'Access-Control-Allow-Origin' $http_origin;
          add_header 'Access-Control-Allow-Credentials' "true";
          add_header Access-Control-Allow-Headers
          $http_access_control_request_headers;
          add_header Access-Control-Allow-Methods GET,PUT,DELETE,POST,OPTIONS;
          return 200;
      }

      if (!-e $request_filename) {
        rewrite  ^/(.+?\.php)/?(.*)$  /$1/$2  last;
        rewrite  ^/(.*)$  /index.php/$1  last;
      }
    }
    location ~ \.php($|/){
        fastcgi_index   index.php;
        fastcgi_pass    unix:/var/run/php/php7.3-fpm.sock;
        include         fastcgi_params;

        add_header 'Access-Control-Allow-Origin' $http_origin;
        add_header 'Access-Control-Allow-Credentials' "true";
        add_header Access-Control-Allow-Headers
        $http_access_control_request_headers;

        add_header Access-Control-Allow-Methods GET,DELETE,POST,OPTIONS;

        set $real_script_name $fastcgi_script_name;
        if ($real_script_name ~ "^(.+?\.php)(/.+)$") {
            set $real_script_name $1;
        }
        fastcgi_split_path_info ^(.+?\.php)(/.*)$;
        fastcgi_param   PATH_INFO               $fastcgi_path_info;
        fastcgi_param   SCRIPT_NAME             $real_script_name;
        fastcgi_param   SCRIPT_FILENAME         $document_root$real_script_name;

      }

    location ~ /\.ht {
        deny all;
    }

   # ssl_certificate     /etc/nginx/ssl/test.think.cn.crt;
   # ssl_certificate_key /etc/nginx/ssl/test.think.cn.key;
}


```
