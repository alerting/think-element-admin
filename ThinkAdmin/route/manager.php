<?php
/**
 * Created by PhpStorm.
 * User: gofollowmymaster
 * Date: 2020/03/06
 * Time: 18:26
 * description:描述
 */
use think\facade\Route;


Route::group('manager', function () {



    //菜单
    Route::resource('page','page');
    Route::get('page/fathermenus', 'page/fatherMenus');
    Route::get('page/forbid/:id', 'page/forbid');
    //角色
    Route::resource('role','role')->except(['create']);
    Route::post('role/forbid/:id', 'role/forbid');
    Route::get('role/getAuthNode/:id', 'role/getAuthNode');
    Route::post('role/saveAuthNode/:id', 'role/saveAuthNode');
    //用户
    Route::resource('user','user');
    Route::post('user/forbid/:id', 'user/forbid');
    Route::post('user/authorize/:id', 'user/authorize');
    Route::post('user/pass/:id', 'user/pass');
    //节点
    Route::get('node$', 'node/index');
    Route::get('node/:id/edit', 'node/edit');
    Route::put('node/:id', 'node/update');
    Route::post('node/forbid/:id', 'node/forbid');
    Route::delete('node/:id', 'node/delete');
    Route::get('node/autoAdd/[:group]', 'node/autoAdd');
    Route::get('node/clear/[:group]', 'node/clear');
    Route::get('node/menunodes', 'node/menuNodes');

    //其他
    Route::post('out', 'Index/out');
    Route::get('php', 'Index/php');
    Route::get('report', 'Index/report');
    Route::post('index/pass', 'Index/pass');
    Route::get('info', 'index/info');
    Route::post('info', 'index/update');
    Route::get('index/menus', 'index/menus');
    Route::get('routes', 'index/routes');

    Route::post('login', 'Login/index');
    //miss
    Route::miss('Login/miss');
})->middleware(\app\manager\middleware\Auth::class)->prefix("manager/")
    ->pattern(['id' => '\d+','group'=>'\w+']);



