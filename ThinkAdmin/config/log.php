<?php


return [
    'type'        =>  env('log.type','file'),
    'apart_level' => ['error', 'sql'],
    'project' => 'open',
    'LogHost' => env('log.loghost', '127.0.0.1'),
    'LogPort' => env('log.logport', '9601'),
];
