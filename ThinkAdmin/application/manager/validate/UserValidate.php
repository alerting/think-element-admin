<?php

/**
 * Created by PhpStorm.
 * User: gofollowmymaster
 * Date: 2020/03/06
 * Time: 18:26
 * description:描述
 */

namespace app\manager\validate;

use think\Db;
use think\exception\ValidateException;
use think\Validate;

class UserValidate extends Validate
{

    protected $rule = [
        'id' => 'require|integer',
        'username' => 'chsDash|unique:system_user',
        'desc' => 'chsDash',
        'phone' => 'mobile',
        'mail' => 'email',
        'authorize' => 'checkAuthorize:thinkphp',
    ];

    protected $message = [
        'id.number' => 'ID格式错误！',
        'id.require' => 'ID不能为空！',
        'username.require' => '管理员名不能为空',
        'username.chsDash' => '管理员名格式错误',
        'username.unique' => '管理员名已存在',
        'desc.chsDash' => '描述信息有非法字符!',
        'phone.mobile'     => '手机号格式错误！',
        'mail.email' => '邮箱格式错误！',
    ];


    // 自定义验证规则
    protected function checkAuthorize($roles, $rule, $data = [])
    {
        // $roles = explode(',', $value) ?: [];

        try {
            foreach ($roles as $role) {
                if (!is_numeric($role)) {
                    throw new ValidateException('授权角色错误');
                }
                $result = Db::name('system_role')->where('id', $role)->findOrEmpty();
                if (!$result) {
                    throw new ValidateException('授权角色不存在');
                }
            }
            $result = true;
        } catch (\Throwable $e) {
            $result = false;
            $message = $e->getMessage() ?: '授权角色错误';
        }
        return $result ? true : $message;
    }

    public function sceneCreate()
    {
        return $this->only(['username', 'desc', 'phone', 'mail', 'authorize'])
            ->append('username', 'require')
            ->append('phone', 'require');
    }
    public function sceneAuthorize()
    {
        return $this->only(['id','authorize'])
            ->append('authorize', 'require');
    }
}
