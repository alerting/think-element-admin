<?php
/**
 * Created by PhpStorm.
 * User: gofollowmymaster
 * Date: 2020/03/06
 * Time: 18:26
 * description:描述
 */

namespace app\manager\validate;

use think\Db;
use think\exception\ValidateException;
use think\Validate;

class RoleValidate extends Validate {
    protected $rule =   [
        'id' => 'require|integer',
        'title' => 'chsDash',
        'desc' => 'chsDash',
        'sort' => 'integer',
        'status'=>'in:0,1',
        'nodes'=>'checkAuthorize:thinkphp',
    ];

    protected $message  =   [
        'id.require' => 'ID不能为空!',
        'id.integer' => 'ID格式错误!',
        'title.require' => '角色名不能为空!',
        'title.chsDash' => '角色名格式错误!',
        'desc.chsDash'     => '描述信息有非法字符!',
        'sort.number' => '排序不能为空！',
        'status.in' => '错误的状态！',
    ];
    public function sceneCreate()
    {
        return $this->only(['title', 'desc', 'sort', 'status', ])
            ->append('title', 'require');
    }
    public function sceneAuthorize()
    {
        return $this->only(['nodes'])
            ->append('nodes', 'require');
    }

    
    // 自定义验证规则
    protected function checkAuthorize($nodes, $rule, $data = [])
    {
        // $roles = explode(',', $value) ?: [];

        try {
            foreach ($nodes as $node) {
//                if (!is_numeric($node)) {
//                    throw new ValidateException('授权节点错误:'.$node);
//                }
                $result = Db::name('system_node')->where('node', $node)->findOrEmpty();
                if (!$result) {
                    throw new ValidateException('授权节点不存在:'.$node);
                }
                if (!$result['status']) {
                    throw new ValidateException('授权节点不可用:'.$result['title']);
                }
            }
            $result = true;
        } catch (\Throwable $e) {
            $result = false;
            $message = $e->getMessage() ?: '授权节点错误';
        }
        return $result ? true : $message;
    }

}
