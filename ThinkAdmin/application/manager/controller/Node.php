<?php

namespace app\manager\controller;

use service\DataService;
use app\manager\service\Node as NodeServer;
use app\manager\model\Node as NodeModel;
use think\App;
use think\Controller;

/**
 * 系统功能节点管理
 * Class Node
 */
class Node extends Controller
{

    private $service;

    public function __construct(App $app = null, NodeServer $service)
    {
        parent::__construct($app);
        $this->service = $service;
    }

    /**
     * 显示节点列表
     * @return string
     */
    public function index()
    {
        $group = $this->request->param('group', '');
        $this->service->clearNodes($group);
        $this->service->autoAdd($group);
        $result = $this->service->searchNodes($group);
        
        return $this->jsonReturn(REQUEST_SUCCESS, '操作成功', $result);
    }

    /**
     * 清理无效的节点记录
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function clear($group = '')
    {
        $this->service->clearNodes($group);
        return $this->jsonReturn();
    }

    public function  autoAdd($group = '')
    {
        $this->service->autoAdd($group);
        return $this->jsonReturn();
    }

    public function update($id)
    {
        $param = $this->request->only(['title', 'is_login', 'is_auth', 'status'], 'post');
        $this->validate($param, 'app\manager\validate\NodeValidate');
        // unset($param['action']);
        unset($param['id']);
        $this->service->updateNodeById($id, $param);
        return $this->jsonReturn();
    }
    /**
     * @param $id
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function edit($id)
    {
        $node = NodeModel::where(['id' => $id])->field('id,title,status,is_auth')->findOrEmpty()->toArray();
        isEmptyInDb($node, '不存在的节点');
        return $this->jsonReturn(REQUEST_SUCCESS, '操作成功', $node);
    }
    public function delete($id)
    {
        $this->service->delNodeById($id);
        return $this->jsonReturn();
    }

    public function menuNodes()
    {
        $result = $this->service->getNodesInDb(['is_menu' => 1, 'status' => 1]);
        $result = array_column($result, 'title', 'node');
        $result = array_merge($result, ['#' => '上级菜单']);
        //返回数据
        return $this->jsonReturn(REQUEST_SUCCESS, '操作成功', $result);
    }



    /**
     * 节点禁用
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    protected function forbid($id)
    {
        $this->service->updateNodeById($id, ['status' => 0]);
        //返回数据
        return $this->jsonReturn();
    }
}
