<?php
/**
 * Created by PhpStorm.
 * User: gofollowmymaster
 * Date: 2020/03/06
 * Time: 18:26
 */

namespace app\common\event\listeners;

use app\common\event\events\Event;
use app\common\service\AuthService;
use app\common\service\LogService;
use think\Db;

/**
 * 登陆成功  监听者
 * Class LoginSuccessListener
 * @package app\common\event\Listeners
 */
class LoginSuccessListener extends EventListener {
    protected function _handle(Event $event) {
        $user = $event->eventInfo;

        //session缓存用户信息
        session('user', $user);
        //session缓存用户授权节点
        AuthService::applyAuthNode($user);
        // 更新登录信息
        Db::name('SystemUser')->where(['id' => $user['id']])
          ->update(['login_at'  => Db::raw('now()'),
                    'login_num' => Db::raw('login_num+1'),]);
        //添加登陆日志
        LogService::write('系统管理', '用户登录系统成功');
    }

}
