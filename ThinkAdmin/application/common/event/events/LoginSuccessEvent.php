<?php
/**
 * Created by PhpStorm.
* User: gofollowmymaster
 * Date: 2020/03/06
 * Time: 18:26
 */

namespace app\common\event\events;

class LoginSuccessEvent extends Event {

    public function __construct(array $user) {
        $this->eventInfo = $user;
    }
}
