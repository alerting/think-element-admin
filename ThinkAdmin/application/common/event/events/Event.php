<?php
/**
 * Created by PhpStorm.
* User: gofollowmymaster
 * Date: 2020/03/06
 * Time: 18:26
 */

namespace app\common\event\events;

abstract class Event {
    const logFileName = 'event';   # 事件系统产生的日志文件名
    public $eventInfo;             # 事件内容信息
}
