<?php
/**
 * Created by PhpStorm.
 * User: gofollowmymaster
 * Date: 2020/03/06
 * Time: 18:26
 */

namespace app\common\dataset;

use  app\common\traits\DataSetTrait;
use  app\common\traits\SingletonTrait;

class Auth {
    use SingletonTrait;
    use DataSetTrait;

    private $description   = '用户';
    private $readAbleField = ['user_id', 'user_type', 'appid', 'username', 'phone'];
    private $modifiableField = ['appid'];


    public function setUserInfo(array $data) {
        $this->setData($data);
        return static::$instance;
    }


    public static function user() {
        return self::getInstance()->getData();
    }


}
