<?php
/**
 * Created by PhpStorm.
 * User: gofollowmymaster
 * Date: 2020/03/06
 * Time: 18:26
 */

namespace app\common\dataset;

use  app\common\traits\DataSetTrait;
use  app\common\traits\SingletonTrait;

class Info {
    use SingletonTrait;
    use DataSetTrait {
        DataSetTrait::__get as  getValue;
    }

    private $description     = '信息';
    private $readAbleField   = ['id', 'data_id', 'data_name', 'data_type_id', 'appid', 'data_plan_id',];
    private $modifiableField = ['appid'];

    public function setDeviceInfo(array $data) {
        $this->setData($data);
        return static::$instance;
    }

    public static function data() {
        return self::getInstance()->getData();
    }

    public function __get($name) {
        if (!($value = $this->getValue($name)) && $this->ext_attr) {
            $value = json_decode($this->ext_attr,true)[$name];
        }
        return $value;
    }
}
