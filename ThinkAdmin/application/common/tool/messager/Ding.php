<?php
/**
 * Created by PhpStorm.
 * User: gofollowmymaster
 * Date: 2020/03/06
 * Time: 18:26
 * description:描述
 */

namespace app\common\tool\messager;

use app\common\tool\Http;

class Ding extends Messager {
    private $client;
    private $config;
    private $gate = "https://oapi.dingtalk.com/robot/send?access_token=";

    public function __construct(array $config) {
        $this->client = new Http();
        $this->config = $config;
    }

    public function handle(string $message,$destination) {

        $token=$this->config[$destination]['token'];
        $url = $this->gate . $token;
        $options['header']=["Content-Type:application/json; charset=utf-8"];
        $options['timeout']=5;
        return $this->client->post($url,$message,$options);
    }
}
