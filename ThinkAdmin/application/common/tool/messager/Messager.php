<?php
/**
 * Created by PhpStorm.
 * User: gofollowmymaster
 * Date: 2020/03/06
 * Time: 18:26
 * description:描述
 */

namespace app\common\tool\messager;

abstract class Messager {

    abstract public function handle(string $message,$destination) ;
}
