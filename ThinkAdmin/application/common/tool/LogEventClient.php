<?php


namespace service;


use app\common\traits\SingletonTrait;

class LogEventClient
{
    use SingletonTrait;

    private $config = [];
    const MESSAGE_QUEUE = 'logEvent:queue:';
    const QUEUE_VOLUME  = 1000;

    private $redis;

    private function __construct(array $config=[])
    {
        $this->config = $config?:config('logEvent');
    }

    public function pushEvent($topic, $content, $channel = 'rpc')
    {

        $messageQueue = self::MESSAGE_QUEUE . $topic;
        if (!$this->isPassListVolume($messageQueue)) {
            $res = $this->getRedis()->lPush($messageQueue, json_encode($content));
        } else {
            $message = ['controller' => "LogController", 'method' => "consumeFromRequest",
                "params" => ["topic" => $topic, 'message' => $content]];
            $res = tcpPost(json_encode($message), $this->config['LogHost'], $this->config['LogPort']);
            $res = json_decode($res, true);
        }
        if (!$res || $res['code']) {
            logToFile('异步事件推送失败' . ($res['msg'] ?? '') . 'message=' . json_encode($content), 'logEvent');
            throw new \Exception('异步事件推送失败' . $res['msg'] ?? '');
        }
        return true;
    }

    private function getRedis()
    {
        if ($this->redis) {
            return $this->redis;
        }
        $config['master'][0] = $this->config['QueueServer'];
        if (empty($config)) {
            //todo 日志服务中出现异常需要记录本地强制日志
            $config = [];
        }
        return $this->redis = redis($config);
    }

    private function isPassListVolume($key)
    {
        if ($this->getRedis()->lLen($key) >= self::QUEUE_VOLUME) {
            return true;
        }
        return false;
    }
}
