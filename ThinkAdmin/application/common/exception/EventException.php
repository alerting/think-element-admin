<?php
/**
 * Created by PhpStorm.
 * User: gofollowmymaster
 * Date: 2020/03/06
 * Time: 18:26
 */

namespace app\common\exception;

use think\Exception;

class EventException extends Exception
{
    protected $code = EVENT_EXCEPTION;
}
