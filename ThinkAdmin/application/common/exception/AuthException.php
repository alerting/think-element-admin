<?php
/**
 * Created by PhpStorm.
 * User: gofollowmymaster
 * Date: 2020/03/06
 * Time: 18:26
 */

namespace app\common\exception;

class AuthException extends IngoreReportException {
    protected $code = AUTH_EXCEPTION;
}
