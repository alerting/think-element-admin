<?php
/**
 * Created by PhpStorm.
 * User: gofollowmymaster
 * Date: 2020/03/06
 * Time: 18:26
 * description:描述
 */

namespace app\common\exception;


class DbException extends \think\exception\DbException
{
    protected $code = DB_EXCEPTION;
}
