<?php
/**
 * Created by PhpStorm.
 * User: gofollowmymaster
 * Date: 2020/03/06
 * Time: 18:26
 */
namespace app\common\exception;

class ConfigException  extends \Exception{

    protected $code=CONFIG_EXCEPTION;
}
