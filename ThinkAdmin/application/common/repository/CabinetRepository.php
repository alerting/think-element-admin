<?php
/**
 * Created by PhpStorm.
 * User: gofollowmymaster
 * Date: 2020/03/06
 * Time: 18:26
 */

namespace app\common\repository;

use app\common\exception\WarningException;

class CabinetRepository extends RedisDriver {

    const USER_USING_CABINET_NO     = "<<appid>>:<<userId>>:cabinetNumber";
    const TODAY_CABINET_USiNG_LOG   = "cabinet:<<appid>>:<<dataId>>:using:log";
    const CABINET_USING_SET         = "cabinet:<<appid>>:<<dataId>>:using";
    const CABINET_FREE_SET          = "cabinet:<<appid>>:<<dataId>>:free";
    const PRIORITY_CABINET_ALL_SET  = "cabinet:<<appid>>:<<dataId>>:priority:<<num>>:template";
    const PRIORITY_CABINET_FREE_SET = "cabinet:<<appid>>:<<dataId>>:priority:<<num>>:free";
    const CABINET_FREEZE_SET        = "cabinet:<<appid>>:<<dataId>>:freeze";

    const BUS_CLEAN_CABINET_CONFIG = 'need:clean:cabinet:bussiness';

    public function getCabinetFree(string $appid, string $dataId) {
        $key = $this->buildRedisKey(self::CABINET_FREE_SET, $appid, $dataId);
        return $this->sMembers($key);
    }

    public function getUserUsingCabinetNo(string $appid, int $userId) {
        $key = $this->buildRedisKey(self::USER_USING_CABINET_NO, $appid, $userId);
        return $this->get($key);
    }

    public function getUsingCabinetNo(string $appid, string $dataId) {
        $key = $this->buildRedisKey(self::CABINET_USING_SET, $appid, $dataId);
        return $this->sMembers($key);
    }

    public function getFreeCabinetNo(string $appid, string $dataId) {
        $key = $this->buildRedisKey(self::CABINET_FREE_SET, $appid, $dataId);
        return $this->sMembers($key);
    }

    public function getFreeCabinetNoByPriority(string $appid, string $dataId, int $priority) {
        $key = $this->buildRedisKey(self::PRIORITY_CABINET_FREE_SET, $appid, $dataId, $priority);
        return $this->sMembers($key);
    }

    public function getFreezeCabinetNo(string $appid, string $dataId) {
        $key = $this->buildRedisKey(self::CABINET_FREEZE_SET, $appid, $dataId);
        return $this->sMembers($key);
    }

    public function isUsing(string $appid, string $dataId,  $cabinetNo) {
        $key = $this->buildRedisKey(self::CABINET_USING_SET, $appid, $dataId);
        return $this->sIsMember($key, $cabinetNo);
    }

    public function isFreeze(string $appid, string $dataId,  $cabinetNo) {
        $key = $this->buildRedisKey(self::CABINET_FREEZE_SET, $appid, $dataId);
        return $this->sIsMember($key, $cabinetNo);
    }

    public function isFree(string $appid, string $dataId,  $cabinetNo) {
        $key = $this->buildRedisKey(self::CABINET_FREE_SET, $appid, $dataId);
        return $this->redis->sIsMember($key, $cabinetNo);
    }

    public function rentCabinet($userId, $dataId, $appid, $cabinetNo) {

        $keys['cabinetFreeSetKey'] = $this->buildRedisKey(self::CABINET_FREE_SET, $appid, $dataId);
        $keys['cabinetPriority1FreeSetKey'] = $this->buildRedisKey(self::PRIORITY_CABINET_FREE_SET, $appid, $dataId, 1);
        $keys['cabinetPriority2FreeSetKey'] = $this->buildRedisKey(self::PRIORITY_CABINET_FREE_SET, $appid, $dataId, 2);
        $keys['cabinetPriority3FreeSetKey'] = $this->buildRedisKey(self::PRIORITY_CABINET_FREE_SET, $appid, $dataId, 3);
        $keys['cabinetUsingSetKey'] = $this->buildRedisKey(self::CABINET_USING_SET, $appid, $dataId);
        $keys['userUsingCabinetNoKey'] = $this->buildRedisKey(self::USER_USING_CABINET_NO, $appid, $userId);
        $needClear = $this->sIsMember(self::BUS_CLEAN_CABINET_CONFIG, $appid) ?: false;
        $this->pipeLine(function ($pipe) use ($keys, $dataId, $cabinetNo, $needClear) {
            $pipe->sRem($keys['cabinetFreeSetKey'], $cabinetNo);
            $pipe->sRem($keys['cabinetPriority1FreeSetKey'], $cabinetNo);
            $pipe->sRem($keys['cabinetPriority2FreeSetKey'], $cabinetNo);
            $pipe->sRem($keys['cabinetPriority3FreeSetKey'], $cabinetNo);
            $pipe->sAdd($keys['cabinetUsingSetKey'], $cabinetNo);
            $pipe->set($keys['userUsingCabinetNoKey'], $dataId . '|' . $cabinetNo);
            if ($needClear) {
                $tomorrow = strtotime(date('Y-m-d', strtotime('+1 day')));
                $pipe->expireAt($keys['userUsingCabinetNoKey'], $tomorrow + 10800);
            }
        });
    }



}
