<?php
/**
 * Created by PhpStorm.
 * User: gofollowmymaster
 * Date: 2020/03/06
 * Time: 18:26
 */
namespace app\common\traits;


Trait SingletonTrait{

    private static $instance;

    private function __construct($data) {

    }

    /**
     * 获取认证实例
     * @param $busId
     * @return DeviceGateRule
     * @throws \Exception
     */
    public static function getInstance( $data=[]) {
        if (empty(self::$instance)) {
            self::$instance = new self($data);
        }
        return self::$instance;
    }

    private function __clone() {
    }
}
