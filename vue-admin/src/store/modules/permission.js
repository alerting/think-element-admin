// import { SUPER_ADMIN_ID } from '@/const/global'
import { getRoutes, getMenus } from '@/api/user'

const state = {
  routes: [],
  addRoutes: [],
  menus: [],
  topMenuIndex: 0
}

const mutations = {
  SET_MENUS: (state, menus) => {
    state.menus = menus
  },
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes
    state.routes = [].concat(routes)
  },
  SET_TOPMENU: (state, menuIndex) => {
    state.topMenuIndex = menuIndex
  }
}

const actions = {
  getRoutes({ commit }) {
    return new Promise(resolve => {
      getRoutes().then(response => {
        const { data } = response
        commit('SET_ROUTES', data)
        resolve(data)
      })
    })
  },
  getMenus({ commit }) {
    return new Promise(resolve => {
      getMenus().then(response => {
        const { data } = response
        commit('SET_MENUS', data)
        resolve(data)
      })
    })
  }

}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
