import { login, logout, getInfo } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
// import router, { resetRouter } from '@/router'
import { SUPER_ADMIN_ID } from '@/const/global'
const state = {
  token: getToken(),
  name: '',
  avatar: '',
  desc: '',
  roles: []

}

const mutations = {
  SESSION_TOKEN: (state, token) => {
    state.token = token
  },
  SET_DESC: (state, desc) => {
    state.desc = desc
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
  }

}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { username, password } = userInfo
    return new Promise((resolve, reject) => {
      login({ username: username.trim(), password: password }).then(response => {
        const { data } = response
        commit('SESSION_TOKEN', data.sessionToken)

        setToken(data.sessionToken)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getInfo(state.token).then(response => {
        const { data } = response

        if (!data) {
          reject('Verification failed, please Login again.')
        }

        const { id, authorize, username, avatar, desc } = data

        // roles must be a non-empty array
        if ((!authorize || authorize.length <= 0) && SUPER_ADMIN_ID !== id) {
          reject('getInfo: roles must be a non-null idstring!')
        } else {
          commit('SET_ROLES', authorize)
          commit('SET_NAME', username)
          commit('SET_AVATAR', avatar)
          commit('SET_DESC', desc)
          resolve(data)
        }
      }).catch(error => {
        reject(error)
      })
    })
  },

  // user logout
  logout({ commit, state, dispatch }) {
    return new Promise((resolve, reject) => {
      logout(state.token).then(() => {
        commit('SESSION_TOKEN', '')
        // commit('ACCESSABLE_NODES', [])
        commit('SET_ROLES', [])
        commit('SET_NAME', null)
        commit('SET_AVATAR', null)
        commit('SET_DESC', null)
        commit('permission/SET_TOPMENU', 0, { root: true })
        commit('permission/SET_MENUS', [], { root: true })
        commit('permission/SET_ROUTES', [], { root: true })
        removeToken()

        // reset visited views and cached views
        // to fixed https://github.com/PanJiaChen/vue-element-admin/issues/2485
        dispatch('tagsView/delAllViews', null, { root: true })

        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      commit('SESSION_TOKEN', '')
      commit('SET_ROLES', [])
      commit('SET_NAME', null)
      commit('SET_AVATAR', null)
      commit('SET_DESC', null)
      commit('SET_MENUS', [])
      commit('SET_ROUTES', [])
      commit('SET_TOPTMENU', 0)
      removeToken()

      resolve()
    })
  }

}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
