import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/manager/page',
    method: 'get',
    params: query
  })
}

export function fetchinfo(id) {
  return request({
    url: '/manager/page/' + id + '/edit',
    method: 'get'
  })
}

export function fetchPmenus(pv) {
  return request({
    url: 'manager/page/fatherMenus',
    method: 'get'
  })
}

export function create(data) {
  return request({
    url: '/manager/page',
    method: 'post',
    data
  })
}

export function update(id, data) {
  return request({
    url: '/manager/page/' + id,
    method: 'put',
    data
  })
}

export function del(id) {
  // const data={}
  // data._method='DELETE'
  return request({
    url: '/manager/page/' + id,
    method: 'delete'
  })
}
