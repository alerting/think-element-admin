import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/manager/node',
    method: 'get',
    params: query
  })
}

export function fetchinfo(id) {
  return request({
    url: '/manager/node/' + id + '/edit',
    method: 'get'
  })
}

export function fetchPv(pv) {
  return request({
    url: '/manager/node/pv',
    method: 'get',
    params: { pv }
  })
}

export function create(data) {
  return request({
    url: '/manager/node',
    method: 'post',
    data
  })
}

export function update(id, data) {
  data._method = 'PUT'
  return request({
    url: '/manager/node/' + id,
    method: 'put',
    data
  })
}

export function del(id) {
  var data
  data._method = 'DELETE'
  return request({
    url: '/manager/node/' + id,
    method: 'post',
    data
  })
}
