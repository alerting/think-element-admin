import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/manager/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/manager/info',
    method: 'get'
  })
}

export function getMenus(token) {
  return request({
    url: '/manager/index/menus',
    method: 'get'
  })
}

export function getRoutes(token) {
  return request({
    url: '/manager/routes',
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/manager/out',
    method: 'post'
  })
}

export function fetchList(query) {
  return request({
    url: '/manager/user',
    method: 'get',
    params: query
  })
}

export function fetchinfo(id) {
  return request({
    url: '/manager/user/' + id + '/edit',
    method: 'get'
  })
}

export function fetchPv(pv) {
  return request({
    url: '/manager/user/pv',
    method: 'get',
    params: { pv }
  })
}

export function create(data) {
  return request({
    url: '/manager/user',
    method: 'post',
    data
  })
}

export function update(id, data) {
  return request({
    url: '/manager/user/' + id,
    method: 'put',
    data
  })
}
export function authorize(id, data) {
  return request({
    url: '/manager/user/authorize/' + id,
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: '/manager/user/' + id,
    method: 'delete'
  })
}
