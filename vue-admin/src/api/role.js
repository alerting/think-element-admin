import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/manager/role',
    method: 'get',
    params: query
  })
}

export function fetchinfo(id) {
  return request({
    url: '/manager/role/' + id + '/edit',
    method: 'get'
  })
}

export function fetchRoleNodes(id) {
  return request({
    url: '/manager/role/getAuthNode/' + id,
    method: 'get'
  })
}

export function saveAuthNode(id, data) {
  console.log('request', data)
  return request({
    url: '/manager/role/saveAuthNode/' + id,
    method: 'post',
    data
  })
}

export function create(data) {
  return request({
    url: '/manager/role',
    method: 'post',
    data
  })
}

export function update(id, data) {
  return request({
    url: '/manager/role/' + id,
    method: 'put',
    data
  })
}

export function del(id) {
  return request({
    url: '/manager/role/' + id,
    method: 'delete'
  })
}
