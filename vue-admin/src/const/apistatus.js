
export const REQUEST_SUCCESS = 0 // 操作成功
export const UNKOWN_ERROR = 90000 // 未知异常

export const AUTH_ERROR = 10000 // 权限异常
export const LOGIN_ERROR = 10001 // 登陆失败
export const HAS_LOGINED_ERROR = 10002 // 你已经登陆
export const NOT_LOGIN = 10003 // 没有登陆
export const ACCESS_NODE_FAIL = 10004 // 没有访问节点权限

export const CONFIG_ERROR = 20000 // 配置异常

export const EVENT_ERROR = 30000 // 事件异常

export const GRPC_ERROR = 40000 // GRPC请求异常

export const PARAM_ERROR = 50000 // 参数异常

export const WARNING_ERROR = 60000 // 参数异常

export const DB_ERROR = 70000 // 参数异常
export const DATANOTFOUND_ERROR = 70001 // 参数异常
