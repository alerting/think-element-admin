
## [Think-Element-admin](http://think-element-admin-fe.laraveler.club)
一个前后端分离的管理后台模板，前端采用[vue-element-admin](https://panjiachen.github.io/vue-element-admin-site/zh/)项目实现,后端采用[thinkphp5.1](https://gitee.com/7788aaa/TP5.1)实现。
实现了基本的RBAC基于角色的权限管理系统，可用与各种后台管理项目，也可以作为PHP开发学习VUE使用。


## 安装步骤
- 克隆项目`git clone https://gitee.com/alaraveler/think-element-admin.git`
- 前端依赖安装  进入vue-admin目录(cd vue-admin) 安装依赖  npm install
> ### 可能出现的问题:   
> - npm ERR! code ENOENT
npm ERR! errno -4058
npm ERR! syscall chmod
>   #### 解决方法:  
> 1. `npm install js-beautify@1.6.14`
> 2. `npm install cnpm -g --registry=https://registry.npm.taobao.org cnpm`
> 3. `cnpm install`
> - Cannot find module 'core-js/modules/es6.regexp.constructor
>  #### 解决方法:  
> 1. `cnpm install core-js@2`
- 后端依赖安装  进入Thinkadmin目录(cd ThinkAdmin) 安装依赖  `composer install`
- 导入数据库   admin.sql
- 修改项目环境变量  拷贝.example.env   .env   修改其中配置
- 服务器配置  参ThinkAdmin目录下README.md
- `npm run dev`；
